FROM registry.gitlab.com/jstijhuis/golang-glock AS build

RUN apk -U add build-base openssl

RUN go get github.com/jwilder/docker-gen
WORKDIR src/github.com/jwilder/docker-gen
RUN make get-deps
RUN make

FROM alpine

ENV DOCKER_HOST unix:///tmp/docker.sock

COPY --from=build /go/src/github.com/jwilder/docker-gen/docker-gen /usr/local/bin/
RUN mkdir -p /etc/docker-gen/templates/ && wget -qO/etc/docker-gen/templates/nginx.tmpl https://raw.githubusercontent.com/jwilder/nginx-proxy/master/nginx.tmpl

ENTRYPOINT ["/usr/local/bin/docker-gen"]
